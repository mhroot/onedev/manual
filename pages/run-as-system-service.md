This document explains how to configure OneDev to run as system service so that it runs in the backend and can start automatically when system starts

### On Windows Platform
1. Edit file `<OneDev dir>\conf\wraper.conf` to set value of property `wrapper.java.command` as path to java command if it does not exist in system path

1. [Open a command prompt with administrator privilege](https://www.howtogeek.com/194041/how-to-open-the-command-prompt-as-administrator-in-windows-8.1/) and switch to folder `<OneDev dir>\bin`

1. Run command `server.bat install`. A Windows service with name _OneDev_ will be installed

1. To uninstall the service, run command `server.bat remove` from the same folder with administrator privilege

### On Linux and Mac OS X

1. Edit file `<OneDev dir>/conf/wraper.conf` to set value of property `wrapper.java.command` as path to java command if it does not exist in system path

1. By default, the service will run under root user. To run as another user, edit file `<OneDev dir>/bin/server.sh` and uncomment below line to specify the user. Make sure specified user has full permissions to OneDev directory and all its sub directories:

  ```
  #RUN_AS_USER=
  ```
1. Run command `sudo <OneDev dir>/bin/server.sh install` to install the service

1. To uninstall the service, run command `sudo <OneDev dir>/bin/server.sh remove`