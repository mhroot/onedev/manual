# Tutorials
------

### Git
* [Annotate source view and diff view with Jest/ESLint information](https://robinshen.medium.com/annotate-source-with-jest-eslint-information-in-onedev-c622641caa45)
* [Hands-on GitOps with OneDev and Kubernetes](https://robinshen.medium.com/hands-on-gitops-with-onedev-f05bd278f07c)
* [Get notification when files under folder _src/core_ are changed on master branch](commit-notification.md)
* [Set up Repository Mirror with GitHub](https://medium.com/p/acf8b85fc69e)
* [Code review with OneDev](https://faun.pub/code-review-with-onedev-c27539366568)

### Pull Request

* [Run CI job against merged commit when pull request changed files under _src_ folder](pull-request-build.md)
* [Require at least two members from expert group to approve if a pull request changed files under _src/core_ folder](pull-request-reviewer.md)
* [Fine-grained notification control](fine-grained-notification-control.md)
* [Set up application live Preview in pull request](https://robinshen.medium.com/live-preview-your-app-in-onedev-pull-request-65c94552c6f5)

### Issue

* [Fix issues via commit message to link issues, commits, builds and pull requests](fix-issues-via-commit-message.md)
* [Add custom field _Platform_ and _Kernel Version_. _Kernel Version_ should be displayed only when _Platform_ is specified as _Linux_](issue-conditional-field.md)
* [Continue with above scenario. For all open Windows issues assigned to tommy or jerry, reassign to robin](batch-assign.md)
* [Create an issue board to show open issue assignment between different users](assignment-board.md)
* [Assign newly created issues to project owners by default](assign-issues-to-project-ownes.md)
* [Add custom field _Module_ and assign issue of particular module to module leader automatically](assign-issues-to-module-leader.md)
* [Auto-transit issue to custom state _Deployed_ when build fixing it is deployed](issue-deployed.md)
* [Auto-transit issue to custom state _In Review_ when relevant pull request is opened](issue-in-review.md)
* [Auto-transit open issues to state _Idle_ when there is no activity on them for some time](noactivity-issue-transition.md)
* [Add custom state _Verified_. Only Tester role can transit issues to this state. Further, the transition is applicable for all issue types except Task](issue-verified.md)
* [Fine-grained notification control](fine-grained-notification-control.md)
* [Create/discuss issues via email](https://robinshen.medium.com/work-with-onedev-service-desk-e56d62c27e57)
* [Working with Issue Links](https://robinshen.medium.com/working-with-issue-links-in-onedev-6bbf94ec0547)
* [Set up Issue Labels](https://robinshen.medium.com/set-up-onedev-issue-labels-3c2ad51488ee)

### Build

* [Build docker image and push to docker hub](build-image.md)
* [Working with CI/CD cache](https://robinshen.medium.com/working-with-onedev-cache-5d84cefc9a3)
* [Build promotion explained](https://robinshen.medium.com/build-promotion-with-onedev-ce6c275aa1b8)
* [Interact with insecure docker registry](insecure-docker-registry.md)
* [Set up CI/CD farm with agents](https://robinshen.medium.com/effortless-onedev-ci-cd-farm-with-agents-3f51ca5a659f)
* [Execute CI/CD jobs outside container with OneDev shell executors](https://robinshen.medium.com/plain-old-builds-with-onedev-shell-executors-989c4c8187cf)
* [Specify build version when manually trigger a build and create a corresponding tag if build is successful](manual-build-version.md)
* [Use POM project version as build version in Java maven project](pom-build-version.md)
* [Prompt for _Platform_ and _Kernel Version_ when manually trigger a build. _Kernel Version_ should be displayed only when _Platform_ is specified as _Linux_](build-conditional-param.md)
* [Test against any combination of _Oracle/MySQL_ and _Linux/Windows_ upon master branch committing](matrix-build.md)
* [Auto-create an issue and assign to committer for investigation upon build failure on master branch; Auto-close the issue when subsequent build succeeds again](assign-build-failure-investigator.md)
* [Generate artifacts in one job, and use them in another job](transfer-artifacts-between-jobs.md)
* [Generate files in one job, process each file concurrently in the second job, and collect processed results in the third job](generate-process-collect.md)
* [Retry jobs upon Kubernetes worker node error](retry-job.md)
* [Use MySQL service while running the job](mysql-service.md)
* [Meet build farm in Kubernetes cluster](k8s-build-farm.md)
* [Run build on windows node in Kubernetes cluster](k8s-windows-node.md)
* [Clone submodules in build job via SSH](clone-submodules-via-ssh.md)
* [Retrieve all project tags while running a job](retrieve-tags.md)
* [Commit code in build job and push to OneDev](push-in-job.md)
* [Build spec reuse and customization](https://robinshen.medium.com/ci-cd-configuration-reuse-in-onedev-c8110b709614)
* [Understanding pipeline](https://robinshen.medium.com/understanding-onedev-pipeline-db0bb0e54aa7)

### Security

* [How to clone via SSH](ssh-guide.md)
* [Single sign on with GitHub](github-sso.md)
* [Single sign on with Okta and authorize users based on Okta group membership information](okta-sso.md)
* [LetsEncrypt setup in Kubernetes](https://robinshen.medium.com/onedev-with-kubernetes-and-letsencrypt-c63d16a3a31)
* [Use secret value in build spec](build-spec-secret.md)
* [Anonymous user can only access release builds of certain projects](anonymous-access.md)
* [Release builds can only be generated from master branch](release-on-master.md)
* [Only release builds from master branch of certain projects can be deployed into production cluster, all other builds should use the test cluster](production-cluster.md)