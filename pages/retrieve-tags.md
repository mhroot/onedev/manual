### Usage Scenario

By default, OneDev only sends code up to desired commit when running a build job. Sometime it is desirable to retrieve all project tags in order to run command such as _git describe --tags_ 

### How to Set Up

To do it, make sure your image has git installed, and then run below command in your job:
```
git fetch --tags
```
This works as OneDev sets up remote and code access credentials appropriately when running the job