### Usage Scenario

Build a docker image and push to Docker hub

### How to Set Up

We demonstrate the setup with a [tutorial project](https://code.onedev.io/projects/283). It is a react project created with `npx create-react-app`. In this project:

1. A simple [dockerfile](https://code.onedev.io/projects/283/blob/master/Dockerfile) is added to serve build result with Nginx
2. A [CI job](https://code.onedev.io/projects/283/blob/master/.onedev-buildspec.yml?position=buildspec-jobs/CI) is added in the build spec to build and publish the docker image. In this job:
  
  * the `checkout` step clones he repository into job workspace
  * the `build` step tests and creates production build
  * the `publish docker image` step builds and publishes the docker image

3. This job should be executed by a docker aware executor (server docker executor, remote docker executor, or kubernetes executor), with the _mount docker sock_ option enabled like below:

  ![docker-executor.png](../images/build-publish-docker-image/docker-executor.png)

  ![specify-job-executor.png](../images/build-publish-docker-image/specify-job-executor.png)  

4. You may also use the built-in _build docker image_ step to do the job. In that case, you will need to specify docker registry login in job executor setting