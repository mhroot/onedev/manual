# Variables

### Substitution Rule

Variable can be inserted into various fields of project build spec by surrounding with _@_. The list of available variables will be prompted when _@_ is typed. Below is some examples of variable substitutions (assume project name is _foobar_):

|<div style='width: 220px;'> Original</div> | Substitued | Explanation|
|----------          |------------|---|
|ubuntu-@project_name@ | ubuntu-foobar | contents enclosed with _@_ is variable and will be substituted|
|ubuntu-@@project_name@@ | ubuntu-@project_name@ | _@@_ will be interpreted as literal _@_|
|ubuntu-@project_name@@|OneDev will complain about invalid variable reference | _@project_name@_ will be interpreted as a variable reference, and the last _@_ is singled out. The correct form should either be _ubuntu-@project_name@@@_, or _ubuntu-@project_name@@someOtherVar@_|

### Variable Reference

|  <div style='width:200px;'>Variable</div> | Explanation |
|---------|------------|
| project_name | name of current project | 
| project_path | [path of current project](concepts.md#project-path) |
| job_name | name of current job |
| ref | git ref the job is running against. For instance:<ul class='mt-3'><li>if job runs against branch _main_, this value will be _refs/heads/main_</li><li>if job runs against tag _v1_, this value will be _refs/tags/v1_</li></ul>|
| branch | git branch this job is running against. This variable will be empty if job is not running against a branch |
| tag | git tag this job is running against. This variable will be empty if job is not running against a tag |
| commit_hash | git commit hash this job is running against | 
| build_number | serial number of generated build which can be used to reference the build | 
| build_version | version of generated build specified via step `Set Build Version`
| pull_request_number | serial number of the pull request triggering the job as result of pull request open/update |
| property:\<property name\> | value of specified build spec property |
| script:builtin:\<script name\> | run specified builtin groovy script and get the result | 
|script:\<script name>\ | run specified custom groovy script and get the result |