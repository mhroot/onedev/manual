### Usage Scenario

Generate artifacts in job1, and use them in job2

### How to Set Up

1. Publish desired artifacts from job1 like below:

  ![publish-artifacts.png](../images/transfer-artifacts/publish-artifacts.png)
  
2. Make job2 depending on job1:

  ![declare-job-dependency.png](../images/transfer-artifacts/declare-job-dependency.png)
  
With this setup, published artifacts of job1 will be retrieved into workspace of job2, and job2 can define necessary steps to process them.