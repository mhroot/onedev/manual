5-minute Quickstart
---

1. Run below command on Linux/Mac to start OneDev in docker mode:

  ```
  docker run --tty --rm -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd)/onedev:/opt/onedev -p 6610:6610 -p 6611:6611 1dev/server
  ```
  If you want to try on Windows machine, please follow the [bare metal installation guide](run-on-bare-metal-machine.md)

1. Point your browser to `http://localhost:6610` to set up OneDev. In system setting page, just use suggested server url (`http://localhost:6610`)

1. From OneDev projects page, add a project _my-app_

1. Run below command from your terminal to create a react application:

  ```
  npx create-react-app my-app
  ```
  
1. Change into directory _my-app_, and run below command to push code to OneDev:

  ```
  git remote add origin http://localhost:6610/my-app
  git push origin master:master
  ```
  When prompted for authentication, input administrator user name and password specified above
  
1. Visit files page of project _my-app_ from OneDev, click link _add build spec_ to bring up the GUI to add build specification. For typical projects, OneDev suggests default job templates like below:

  ![Add Job Wizard](../images/add-job-wizard.png)
  
1. Just use the default template, and save the spec. Now you will see that a CI build is running:

  ![After Add Ci Job](../images/after-add-ci-job.png)
    
1. Congrats! You've finished the tutorial. Continue to check [more tutorials](tutorials.md) if you are interested.