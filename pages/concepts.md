# Concepts
------

### Project

Project is core concept of OneDev, and can be used to hold code, issues, builds, etc. Projects can be organized hierarchically to reflect parent-child relationship as well as facilitate setting inheritance. 

### Project Path

If a project does not have parent, its project path will be project name; otherwise, its project path is <span class='text-nowrap'>`<parent project path>/<project name>`</span>. Assume below project hierarchy:
```
projectA
    projectB 
        projectC
    projectD
```
Project path of `projectC` will be `projectA/projectB/projectC`

### Role

Role defines a set of permissions. When a project is authorized to a user or group, the role must be specified to determine the actual permissions 

### Pull Request

Like many other git products, OneDev has pull request for work submission and code review. Code review policies can be defined for instance to specify required reviewers when certain files are changed. 

### Build Spec

Build spec describes specification of build processes in OneDev. It is defined in file _onedev-buildspec.yml_ in root of the repository

### Job

Jobs are defined in build spec and a single job runs on a single node. Jobs can depend on each other to accomplish complex build workflows such as concurrent cross-node processing. Job can run automatically with job triggers, or manually from desired commits

### Job Trigger

Job trigger defined when a job should be triggered. Below triggers are currently available:

* Branch update
   Triggers when new commits are pushed to specified branches
* Tag creation
   Triggers when specified tags are created
* Pull request open or update
   Triggers when specified pull requests are opened or updated
* Pull request merge
   Triggers when specified pull requests are merged
* Pull request discard
   Triggers when specified pull requests are discarded
* Dependency job finished
   Triggers when dependency job finishes
* Cron schedule
   Triggers when scheduled time reaches

### Job Executor

Job executors are used to run jobs. There are five types:

* Kubernetes Executor
  Run jobs as pods in Kubernetes cluster. No agents required
* Server Docker Executor
  Run jobs inside docker containers on server
* Remote Docker Executor
  Run jobs inside docker containers on remote machines. Agents need to be installed on remote machines
* Server Shell Executor
  Run jobs directly with shell/batch facility on server
* Remote Shell Executor
  Run jobs directly with shell/batch facility on remote machines. Agents need to be installed on remote machines. 
  
### Agent

Agents runs on remote machines to execute jobs dispatched by server. It can be included in one or more remote job executors via agent selector. Depending on the job executor being used, it executes jobs either inside or outside containers. Agents are not required if you run jobs on server or inside Kubernetes cluster

### Job Workspace

Job workspace is the working directory where job commands are executed. Repository and dependency files (if there is any) will be retrieved into this directory. Artifacts/reports will also be published based on this directory

### Step

Steps are defined in job to execute scripts on designated images. Job steps execute sequentially on job node and share the same job workspace

### Step Template

Step template groups a sequence of steps as a whole and can be customized with parameters. A step template can be used in a job and itself can use other step templates if necessary

### Service

Services define live facilities used by a job such as database, message queue etc

### Build

Build is result of running a job

### Build Promotion

If some jobs depend on job of a particular build, this build can then be promoted to run these downstream jobs. During promotion, artifacts (normally verified to be `good`) of current build can be passed to downstream jobs for further processing

### Pipeline

Pipeline is an execution of job dependency graph. Job dependency graph can be executed by running any job in the graph, either manually or automatically via job triggers

### Build Stream

Two builds are on same stream if they:
* Belongs to same project
* Belongs to same job
* Corresponding commits are on same branch