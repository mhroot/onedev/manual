# Run on Virtual Machine/Bare Metal Machine

To achieve maximum flexibility, you may run OneDev on virtual machine or bare metal machine

### Resource Requirement

OneDev can run happily on a 2 core 2GB box. For personal use, 1 core 1GB box also works. In this case, you will need to edit `<OneDev dir>/conf/wrapper.conf` to comment out property `wrapper.java.maxmemory.percent=50` and uncomment `wrapper.java.maxmemory=256m`

### Installation

1. Make sure your system has [Java 8 or higher](https://openjdk.java.net/install/) installed and available in system path 
1. Make sure your system has [git 2.11.1 or higher](https://git-scm.com/downloads) installed and available in system path
1. On Linux and Mac, make sure your system has [curl](https://curl.haxx.se) installed and available in system path
1. Select desired [OneDev release](https://code.onedev.io/projects/160/builds?query=%22Job%22+is+%22Release%22)  and download `onedev-<release>.zip`
1. Extract downloaded file into selected installation directory. Make sure current user has *full permissions* to the directory and all its sub directories
1. Open a command prompt, change to the installation directory, and run command _bin\server.bat console_ (on Windows) or _bin/server.sh console_ (on Unix/Linux/Mac) to start the server

### Post-install tasks (optional)

 * [Increase Ulimit](increase-ulimit.md)
 * [Run as System Service](run-as-system-service.md)
 * [Use External Database](use-external-database.md)