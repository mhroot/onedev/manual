### Usage Scenario

Clone submodules via SSH protocol in a build job

### How to Set Up

1. Make sure you can clone all the submodules locally in your terminal. To make it working, you will need to add **public key** (normally content of file _~/.ssh/id_rsa.pub_) to your account and make sure your account has permission to pull all submodule projects

  ![Add Ssh Key](../images/clone-via-ssh/add-ssh-key.png)
  
2. Edit build setting of the main project to define a job secret containing your **private key** (normally content of file _~/.ssh/id_rsa_) 

  ![Add Private Key](../images/clone-via-ssh/add-private-key.png)
  
3. Configure checkout step of the job to use SSH credential and specify secret defined above as private key

  ![Checkout With Ssh](../images/clone-via-ssh/checkout-with-ssh.png)