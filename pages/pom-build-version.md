### Usage Scenario

Use POM project version as build version in Java maven project

### How to Set Up

1. Add a shell/batch step to extract project version from POM and write to a file:

  ![Detect Build Version](../images/pom-build-version/detect-build-version.png)
  
  Full command used in above screenshot:
  ```shell
  echo "Detecting project version (may require some time while downloading maven dependencies)..."
  echo $(mvn org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate -Dexpression=project.version -q -DforceStdout) > buildVersion
  ```
  
2. Add a step of type _Set Build Version_ to set version using content of file _buildVersion_ generated above:

  ![Set Build Version](../images/pom-build-version/set-build-version.png)