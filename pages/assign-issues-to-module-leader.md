### Usage Scenario

Add custom field _Module_ and assign issue of particular module to module leader automatically

### How to Set Up

1. Add custom issue field _Module_ from issue setting of administration menu:

  ![Default Assignee Module](../images/assign-issues-to-module-leader/module-field.png)
  
1. Define a groovy script to return corresponding module leader based on module name

  ![Default Assignee Groovy Script](../images/assign-issues-to-module-leader/groovy-script.png)
  Script content is as below:
  ```groovy
  import io.onedev.server.util.EditContext

def moduleLeaders = ["Front End":"tommy", "Back End":"jerry"]
def editContext = EditContext.get()

def defaultAssignee

if (editContext != null)
	defaultAssignee = moduleLeaders[editContext.getInputValue("Module")]

if (defaultAssignee != null)
	return [defaultAssignee]
else
	return []
  ```
  
1. Modify custom issue field _Assignee_ to use groovy script defined above as default value:

  ![Default Assignee Default Value](../images/assign-issues-to-module-leader/use-groovy-script.png)