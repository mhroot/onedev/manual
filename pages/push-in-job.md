### Usage Scenario

Make code changes in build job, commit the change and push the change to OneDev server

### How to Set Up

1. Make sure your account has code push permission over current project. Switch to my access token page and copy the value to clipboard

  ![Copy Access Token](../images/push-in-job/copy-access-token.png)
  
2. Edit project build setting to add a job secret using the value copied above

  ![Define Job Secret](../images/push-in-job/define-job-secret.png)

3. Configure checkout step of the job to retrieve source with http(s) credential, and choose job secret created above as access token 

  ![Use Customized Clone Credential](../images/push-in-job/use-customized-clone-credential.png)
  
4. Add shell/batch step to call appropriate git commands to commit and push, for instance:

  ![Define Build Step](../images/push-in-job/define-build-step.png)
  
  Commands used in this example step:
  ```shell
  git config --global user.name "Your Name"
  git config --global user.email "you@@example.com"

  # Fetch and checkout master as OneDev by default clones 
  # code up to current commit
  git fetch origin master:master
  git checkout master

  echo @build_number@ > build_number.txt
  git add build_number.txt
  git commit -m "Update build number"
  git push origin master:master
  ```