# Administration Guide
---------
* [Configure Job Executors](configure-job-executors.md)
* [Agent Management](agent-management.md)
* [Backup and Restore](backup-restore.md)
* [Reset Admin Password](reset-admin-password.md)
* [Set up Reverse Proxy](reverse-proxy-setup.md)
* [Set up Https](https-setup.md)
* [Set up SSH](ssh-guide.md)