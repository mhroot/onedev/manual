# Maintenance Mode
----
Some tasks need to be performed in maintenance mode, such as restoring database, resetting administrator password, etc. Below explains how to enter into this mode for different installation flavors:

* If you are [running OneDev as docker container](run-as-docker-container.md):

  1. Stop the container
  1. Run below command to enter into container:

    ```
    $ docker run -it --rm -v <data dir>:/opt/onedev <external database environment variables> 1dev/server bash 
    ```
    - `<data dir>` represents the directory storing OneDev data
    - In case [using external database via environment variables](run-as-docker-container.md#use-external-database), `<external database environment variables>` should be replaced by those environment variables you are used to start OneDev; otherwise just remove it
    
  1. Change into directory _/opt/onedev_ in the container to perform various maintenance tasks
  
* If you are [deplolying OneDev into Kubernetes cluster](deploy-into-k8s.md):
  1. Run below command to put OneDev into maintenance mode:

    ```
    $ helm upgrade onedev onedev/onedev --namespace onedev --set maintenance=true --reuse-values
    ```
  1. Run command `kubectl get pods -n onedev` to check pod name of onedev
  2. Run below command to open a shell to OneDev server (replace _<onedev-pod-name>_ with the actual pod name obtained in previous step):
  ```
  $ kubectl exec -it -n onedev <onedev-pod-name> bash
  ```
  3. Change to directory _/opt/onedev_ in the container to perform various maintenance tasks
  4. After performing maintenance tasks, you may switch OneDev back to normal mode by running below command:

    ```
    $ helm upgrade onedev onedev/onedev --namespace onedev --set maintenance=false --reuse-values
    ```

* If you are [running OneDev on virtual machine/bare metal machine](run-on-bare-metal-machine.md):

  1. Stop OneDev service 
  2. Change to the directory where OneDev is installed to perform maintenance tasks