### Usage Scenario 

Use MySQL service while running job

### How to Set Up

1. Define mysql service in _Services_ tab like below:

  ![Define Mysql Service](../images/mysql-service/define-mysql-service.png)
  
  Note that for MySQL root password, we are referencing secret _db-password_ defined in [this tutorial ](build-spec-secret.md)
  
1. Use defined service in relevant jobs as below:

  ![Use Mysql Service](../images/mysql-service/use-mysql-service.png)
  
1. If the job runs with a Kubernetes executor and if you want to run MySQL  service on particular nodes, configure service locator in section _More Settings_ of the executor like below:

  ![Service Locator](../images/mysql-service/service-locator.png)
  
1. Now in command steps of the job you may connect to database via host name _mysql_ (same as service name)