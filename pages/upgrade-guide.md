# Upgrade Guide
-------

###### If you are [running OneDev as docker container](run-as-docker-container.md):

 1. Stop container if it is still running:
   ```
   $ docker stop onedev
   ```
 1. Delete container:
   ```
   $ docker rm onedev
   ```
 1. Pull latest image:
   ```
   $ docker pull 1dev/server
   ```
 1. Run container again. Wait a while for OneDev to upgrade and start.
  
###### If you are [deploying OneDev into Kubernetes](deploy-into-k8s.md), just run below commands:

```
$ helm repo update
$ helm upgrade onedev onedev --repo https://dl.cloudsmith.io/public/onedev/onedev/helm/charts --namespace onedev --reuse-values

```

###### If you are [running OneDev on virtual machine/bare metal machine](run-on-bare-metal-machine.md):
  1. Assume OneDev is installed at _/opt/onedev_, stop OneDev by running command _/opt/onedev/bin/server.(sh|bat) stop_
  1. Make sure database server is up and running if you are using external database
  1. Extract new OneDev version to a separate folder, say _/path/to/new-onedev
  1. Run below command to perform the upgrade:
    ```
    $ /path/to/new-onedev/bin/upgrade.(sh|bat) /opt/onedev
    ``` 
  1. _/opt/onedev_ will be upgraded and you may launch it to check if things are in order
  1. You may remove _/path/to/new-onedev_ afterwards if everything is fine