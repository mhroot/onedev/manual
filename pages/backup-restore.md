# Backup and Restore
------

### Database Backup

1. Manual backup from GUI
Login to OneDev as administrator, navigate to menu _Administration / Database Backup_, and click the button _Backup Now_
2. Manual backup from command prompt
Switch OneDev to [maintenance mode](maintenance-mode.md) and run below command (assume OneDev is installed in _/opt/onedev_):
  ```
  /opt/onedev/bin/backup-db.(sh|bat) /path/to/backup.zip
  ```
  
3. Schedule auto-backup
Login to OneDev as administrator, navigate to menu _Administration / Database Backup_, and enable auto backup

### Database Restore

To restore database from an existing backup file, switch OneDev to [maintenance mode](maintenance-mode.md) and run below command (assume OneDev is installed in _/opt/onedev_):
```
/opt/onedev/bin/restore-db.(sh|bat) /path/to/backup.zip
```

### Repository Backup

The folder _/opt/onedev/site_ (assume OneDev is installed in _/opt/onedev_) contains git repositories, attachments and other important data. It should be backed up periodically. You may use some volume backup tools or file sync tools for this purpose.