Many objects in OneDev are identified via a path-like structure, for instance, project path _root/project/subproject_, ,branch _bugfixes/2.0_, tag _releases/v2_, file _src/client/menu.js_, or docker image _alpine/git_

When match against such paths, a single wildcard character `*` will match current path segment, for instance:

* `*` will match _file_ but not _dir/file_ 
* `dir/*` will match _dir/file_ but not _file_
* `*/*` will match _dir/file_ but not _dir/dir2/file_

To match against arbitrary path segments, use double `**`, for instance:

* `**` will match all paths recursively
* `dir/**` will match all paths under _dir_ recursively
* `**/*.js` will match all paths ending with _.js_ recursively