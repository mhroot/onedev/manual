# Run as Docker Container
------

### Resource Requirement

OneDev can run happily on a 2 core 2GB box.

### Start OneDev Server
Run below command on Linux/Mac, with `<data dir>` replaced by a directory storing OneDev data:
```
docker run --name onedev -d --restart always -v /var/run/docker.sock:/var/run/docker.sock -v <data dir>:/opt/onedev -p 6610:6610 -p 6611:6611 1dev/server
```
Wait a while for OneDev to get up and running, then point your browser to `http://localhost:6610` to access it.

If you want to run OneDev in foreground. Use below command:
```
docker run -t --name onedev --rm -v /var/run/docker.sock:/var/run/docker.sock -v <data dir>:/opt/onedev -p 6610:6610 -p 6611:6611 1dev/server
```

### Use External Database

OneDev by default uses an embedded database to store data. You may use environment variables below to connect to external database if desired. 

<div class='text-warning mb-3'>(make sure to change url, user and password to fit your needs. The user needs to be able to create/delete/write tables in specified database)</div>

1. MySQL

  * hibernate_dialect=org.hibernate.dialect.MySQL5InnoDBDialect
  * hibernate_connection_driver_class=com.mysql.cj.jdbc.Driver
  * hibernate_connection_url=jdbc:mysql://localhost:3306/onedev?serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false&disableMariaDbDriver=true
  * hibernate_connection_username=root
  * hibernate_connection_password=root

1. PostgreSQL

  * hibernate_dialect=io.onedev.server.persistence.PostgreSQLDialect
  * hibernate_connection_driver_class=org.postgresql.Driver
  * hibernate_connection_url=jdbc:postgresql://localhost:5432/onedev
  * hibernate_connection_username=postgres
  * hibernate_connection_password=postgres
  
1. MariaDB

  * hibernate_dialect=org.hibernate.dialect.MySQL5InnoDBDialect
  * hibernate_connection_driver_class=org.mariadb.jdbc.Driver
  * hibernate_connection_url=jdbc:mariadb://localhost:3306/onedev
  * hibernate_connection_username=root
  * hibernate_connection_password=root

     You also need to download MariaDB JDBC driver and put into directory `<data dir>/site/lib` (this directory will be populated after first run of OneDev)
     
1. MS SQL Server

  * hibernate_dialect=org.hibernate.dialect.SQLServer2012Dialect
  * hibernate_connection_driver_class=com.microsoft.sqlserver.jdbc.SQLServerDriver
  * hibernate_connection_url=jdbc:sqlserver://localhost:1433;databaseName=onedev
  * hibernate_connection_username=sa
  * hibernate_connection_password=sa

     You also need to download SQL server JDBC driver and put into directory `<data dir>/site/lib` (this directory will be populated after first run of OneDev)
 
1. Oracle

  * hibernate_dialect=org.hibernate.dialect.Oracle10gDialect
  * hibernate_connection_driver_class=oracle.jdbc.driver.OracleDriver
  * hibernate_connection_url=jdbc:oracle:thin:@localhost:1521:XE
  * hibernate_connection_username=onedev
  * hibernate_connection_password=onedev

     You also need to download Oracle JDBC driver and put into directory `<data dir>/site/lib` (this directory will be populated after first run of OneDev)