# Deploy into Kubernetes Cluster
--------

### Resource Requirement

A 2 core 4GB work node will be more than enough for OneDev deployment.

### Prerequisite

1. Kubernetes cluster version 1.18 or higher
1. connected to the cluster
1. kubectl and helm3 installed

### Installation

1. Install OneDev chart into cluster:
  ```
  $ helm install onedev onedev --repo https://dl.cloudsmith.io/public/onedev/onedev/helm/charts --namespace onedev --create-namespace
  ```
  [Check here](https://code.onedev.io/projects/160/blob/main/server-product/helm/values.yaml) for configurable helm values
  
1. OneDev will create a load balancer for external access. Run below command to get external ip of the load balancer:
  ```
  $ kubectl get service onedev -n onedev
  ```
1. Pointer your browser to `http://<external ip>` to access OneDev
1. Continue to setup ingress and letsencrypt below if desired

**NOTE:**

* Besides creating resources in namespace _onedev_, a cluster role _onedev_ and associated cluster role binding _onedev_ will be created in default namespace. This is necessary as OneDev needs to be granted some cluster-wide permissions to run builds as pods
* OneDev will create two persistent volume claims, one to store MySQL database files, and another to store OneDev data including repositories. The actual place of these volumes varies depending on your Kubernetes cluster config. On Google Kubernetes Engine, they will be created as Google persistent disks

### Ingress Setup

The installation procedure above requires a separate load balancer which can be expensive. In case the cluster already has an ingress controller serving external traffic, you can configure OneDev to be accessed via virtual host of the ingress controller with below procedure:

1. Get external ip address of the ingress controller. For a default installation of nginx controller, this can be shown with below command:

  ```
  $ kubectl get service ingress-nginx-controller -n ingress-nginx
  ```
1. Configure your DNS provider to add a `A` record to associate OneDev DNS name with external ip address of nginx controller above. 

  **NOTE:** this is *NOT* the ip address of OneDev load balancer we used previously
  
1. Run below command to tell ingress controller to route traffic of above DNS name to OneDev:
  ```
  $ helm upgrade onedev onedev --repo https://dl.cloudsmith.io/public/onedev/onedev/helm/charts --namespace onedev --set ingress.host=<OneDev DNS name> --set ingress.class=<ingress class> --reuse-values
  ```
  Here `<OneDev DNS name>` should be replaced by DNS name specified in step 2, and `<ingress class>` should be replaced by ingress class of desired ingress controller, which is normally `nginx` for nginx controller
  
1. Now you should be able to access OneDev via url `http://<OneDev DNS name>`
1. To enable SSH access to OneDev repositories, you need to configure ingress controller to forward traffic of certain port to OneDev SSH service. For nginx controller, this can be achieved by upgrading controller like below:
  ```
  $ helm upgrade ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace --set tcp.<external SSH port number>=onedev/<OneDev SSH service name>:<OneDev SSH port number> --reuse-values
  ```
  Here:
  - `<external SSH port number>`: port number you wish to expose on nginx controller for external SSH access  
  - `<OneDev SSH service name>`: name of OneDev SSH service, which is normally `onedev`. If you separate SSH service from main service with option `ssh.separateService`, it will be `onedev-ssh`
  - `<OneDev SSH service port>`: port of OneDev SSH service, which is normally `22`, unless overridden via option `ssh.port`

  After running this command, make sure to update SSH server url at OneDev side as `ssh://<OneDev DNS name>:<external SSH port number>` via menu _Administration/System Setting_

### LetsEncrypt Setup

1. Make sure OneDev is installed [following this guide](#installation)

1. Make sure your cluster has ingress controller installed. If not, run below command to install:

  ```
  $ helm install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace
  ```
  
1. Set up ingress for OneDev [following this guide](#ingress-setup)

1. To automatically get/renew certificate from LetsEncrypt, a cert manager is required. Install with below command if the cluster does not have one:

  ```
  $ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.0/cert-manager.yaml
  ```
  
1. Run command below to configure OneDev to request certificate from LetsEncrypt for the DNS name specified previously

  ```
  $ helm upgrade onedev onedev --repo https://dl.cloudsmith.io/public/onedev/onedev/helm/charts --namespace onedev --set ingress.tls=true --set letsencrypt.email=<an email address> --reuse-values
  ```
  Here `<an email address>` should be replaced by an email address used to receive certificate notifications such as invalidation/expiration etc.
  
1. Wait a while, and access OneDev from browser with url `https://<OneDev DNS name>`. If the certificate is invalid, run command below to check the certificate status:

  ```
  $ kubectl describe certificate onedev-tls -n onedev
  ```