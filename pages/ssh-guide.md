OneDev supports to access repositories via SSH protocol since 3.1. 

SSH clone url can be shown as below from root of project page:

  ![ssh-clone-url.png](../images/ssh-clone-url.png)

It uses port 6611 by default, and this can be changed via SSH setting like below:

![ssh-port.png](../images/ssh-port.png)