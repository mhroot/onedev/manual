### Usage Scenario

Auto-transit open issues to state _Idle_ when there is no activity on them for some time

### How to Set Up

1. Switch to page _Administration / Issue Setting_, and define a custom state _Idle_ 

  ![Idle Issue State](../images/idle-issue-state.png)

2. Add a transition rule to transit open issues to idle state 

  ![Transit Noactivity Open Issues](../images/transit-noactivity-open-issues.png)