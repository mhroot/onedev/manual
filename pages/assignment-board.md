### Usage Scenario

Create an issue board to show open issue assignment between different users

### How to Set Up

1. Add field _Assignee_ of type _User_ in _Administration / Issue Setting / Custom Fields_. Make sure the _multiple_ is NOT checked. 

  **NOTE:** OneDev shipped with a default "Assignees" field with _multiple_ option checked, you may turn off that option and use it for below test instead of creating a new one

1. Change to issue boards page of a project, and add a board like below:

  ![Create Issue Board](../images/create-issue-board.png)
  
1. In board setting page, use _Assignee_ as identify field, and choose users as columns

  ![Edit Assignment Board](../images/edit-assignment-board.png)
  
1. Save the setting, and you will see the board in action:

  ![Assignment Board](../images/assignment-board.png)