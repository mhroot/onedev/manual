# Https Setup
-------

If you are [deploying OneDev into Kubernetes with Helm](deploy-into-k8s-with-helm.md), there is a section _LetsEncrypt Setup_ explaining how to do this via LetsEncrypt.  For all other cases, you will need to set up a [reverse proxy](https://code.onedev.io/projects/162/blob/main/pages/reverse-proxy-setup.md) and then configure https for the front end