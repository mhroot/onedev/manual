### Usage Scenario

Specify build version when manually trigger a build and create a corresponding tag if build is successful

### How to Set Up

1. Define a job parameter say _Build Version_ in _Params & Triggers_ section:

  ![Define Build Version Param](../images/manual-build-version/define-build-version-param.png)
  
1. Add a step of type _Set Build Version_ to use above param:

  ![Set Build Version](../images/manual-build-version/set-build-version.png)
 
1. Add a step of type _Create Tag_ to tag the repository with current build version:

  ![Create Tag](../images/manual-build-version/create-tag.png)