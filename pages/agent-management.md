# Agent Management
----------

Agent can be installed on machines where you want to run jobs. Currently Windows, Mac, Linux and FreeBSD are supported.

### Agent Installation

You may install new agents and get it connected to OneDev following instructions here:

![add-agent.png](../images/add-agent.png)

### Agent Upgrade

Agent is designed with zero maintenance in mind. Once installed, it will be updated automatically if server is upgraded 

### Token Management

Agent uses token to authenticate to OneDev server, and a single token can only be used by a single agent. On the agent installation instruction page above, when docker command is shown or agent is downloaded, a new token is automatically generated. You may also generate token manually like below and configure agent to use it:

![agent-tokens.png](../images/agent-management/agent-tokens.png)

For docker based installation, token can be specified via environment `agentToken`, and for bare metal/virtual machine based installation, it can be done via `agentToken` property in `<agent dir>/conf/agent.properties`

### Pause/Resume Agents

You may pause agent to prevent server from sending new jobs to it. Existing jobs will continue to run until finished. 

![pause-agents.png](../images/agent-management/pause-agents.png)

### Running as System Service

<div class='alert alert-light-warning'><b>NOTE:</b> This is only applicable when agent runs on bare metal/virtual machine</div>

* On Windows Platform

  1. Edit file <agent dir>\conf\wraper.conf to set value of property `wrapper.java.command` as path to java command if it does not exist in system path
  1. [Open a command prompt with administrator privilege](https://www.howtogeek.com/194041/how-to-open-the-command-prompt-as-administrator-in-windows-8.1/) and switch to folder `<agent dir>\bin`
  1. Run command `agent.bat install`. A Windows service with name `OneDev Agent` will be installed
  1. To uninstall the service, run command `agent.bat remove` from the same folder with administrator privilege

* On Linux and Mac OS X

  1. Edit file <agent dir>/conf/wraper.conf to set value of property `wrapper.java.command` as path to java command if it does not exist in system path
  1. By default, the service will run under root user. To run as another user, edit file `<agent dir>/bin/agent.sh` and uncomment below line to specify the user. Make sure specified user has full permissions to agent directory and all its sub directories:

    ```
    #RUN_AS_USER=
    ```
  1. Run command `sudo <agent dir>/bin/agent.sh install` to install the service
  1. To uninstall the service, run command `sudo <agent dir>/bin/agent.sh remove`

### Agent Properties/Environments

Below is list of supported properties in `<agent install dir>/conf/agent.properties`. These properties can also be specified via environment variables if agent is launched via docker

| Name | <span class='text-nowrap'>Required</span> | Description |
| ----- | ----- | ---- |
| serverUrl | yes | Url of OneDev server | 
| <span class='text-nowrap'>agentToken</span> | yes if _agentTokenFile_ is not specified | Token to authenticate to OneDev server |
| <span class='text-nowrap'>agentTokenFile</span><br>(since 6.3.12) | yes if _agentToken_ is not specified | File containing token to authenticate to OneDev server |
| <span class='text-nowrap'>agentName</span> | no | Name of agent. Host name will be used if not specified |
| <span class='text-nowrap'>temporalAgent</span> <br> (since 6.3.13)| no | `true` or `false` to indicate whether or not this agent is temporal. A temporal agent will be removed as soon as it goes offline. Default to _false_ if ommitted |
| agentCpu | no | CPU capability of the agent in millis. It is normally `(cpu cores)*1000`. Will be detected automatically if not specified |
| <span class='text-nowrap'>agentMemory</span> | no | Physical memory of the agent in mega bytes. Will be detected automatically if not specified |
| gitPath | no | Path to git command line. Will search in system path if not specified | 
| dockerPath | no | Path to docker command line. Will search in system path if not specified |
