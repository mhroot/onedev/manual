### Usage Scenario

Single sign on with Okta and authorize users based on Okta group membership information

### How to Set Up

#### Single Sign On with Okta

1. Make sure your OneDev instance can be accessed publicly, and configure the public server url in _Administration / System Setting_

  ![Configure Server Url](../images/configure-server-url.png)
  
2. Login to Okta to manage the applications

  ![Okta Your Org](../images/okta/manage-applications.png)
    
3. Add OneDev as a web application to get the client id and secret

  ![Okta Client Credentials](../images/okta/create-application.png)
  
  ![create-app-options.png](../images/okta/create-app-options.png)
  
  ![client-id-and-secret.png](../images/okta/client-id-and-secret.png)
  
4. Switch to _api/authorization servers_ to get the default issuer URL

  ![Okta Issuer Uri](../images/okta/issuer-url.png)
  
5. At OneDev side, switch to page _Administration / Authentication Source / Single Sign On_, add a provider of type _OpenID_ , with information from previous steps

  ![Add Okta Sso](../images/okta/add-okta-sso.png)
  
8. Now sign out and a button _Login with Okta_ will appear at bottom of the login page. Anyone in your Okta organization assigned to OneDev application will be able to login via this button

  ![Okta Login Button](../images/okta/okta-login-button.png)
  
#### Authorize Users Based On Okta Group Membership Information

1. At OneDev side, edit Okta single sign on provider, and specify _groups claim_ as _groups_

  ![Okta Sso Groups Claim](../images/okta/sso-groups-claim.png)
  
2. At Okta side, switch to page _api / authorization servers_, select default authorization server to add _groups_ scope

  ![Okta Add Groups Scope](../images/okta/add-groups-scope.png)

3. Continue to add _groups_ claim to be included in ID token and _groups_ scope like below

  ![Okta Add Groups Claim](../images/okta/add-groups-claim.png)
  
4. At OneDev side, switch to page _Administration / Group Management_, add necessary Okta groups (same name) and assign appropriate permissions

5. Now users signed in via Okta will be added to corresponding groups at OneDev side to get appropriate permissions

#### Access OneDev from Okta Side

To access OneDev from Okta side, edit the application to enable implicit grant type, tick the option _display application icons to users_, and configure initiate login url as below:

![login-from-okta.png](../images/okta/login-from-okta.png)

Then for all users added to the application, they will be able to access OneDev directly from their Okta dashboards:

![okta-my-apps.png](../images/okta/okta-my-apps.png)