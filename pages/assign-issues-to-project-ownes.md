### Usage Scenario

Assign issues to project owners by default

### How to Set Up

1. Define a groovy script to return project owners

  ![Groovy Script](../images/assign-issues-to-project-owners/groovy-script.png)
  Content of the script is as below:
  ```groovy
  import io.onedev.server.model.*

def project = Project.get()

def owners = project.userAuthorizations.findAll {it->it.role.owner}*.user.name

owners += project.groupAuthorizations.findAll {it->it.role.owner}*.group.members.flatten()*.name

return owners.unique()
  ```
  
1. Modify custom issue field _Assignee_ to use groovy script defined above as default value:

  ![Assignee Default Value](../images/assign-issues-to-project-owners/use-groovy-script.png)