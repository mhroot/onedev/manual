# Fine-grained notification control
---

These instructions apply to both: Issues and Pull Requests.

## How notifications work

* An Issue (or Pull Request) has been updated
* OneDev checks the saved queries one after another starting at the top
* If a query matches OneDev stops iterating the queries
* Depending on the query watch setting one of the following rules applies
  * *Watch if involved*: Notification only if watching the issue
  * *Watch*: Notification on any Issue activity
  * *Do not watch*: No notification at all

The query watch setting can be selected by clicking the circle next to the query name:

![](../images/notification-settings.png)

## Examples

### Notifications only for issues that are assigned to me

Receive notifications only for issues that are assigned to me, nothing else.

* Configure a saved query `"Assignee" is me`
* Select **Watch** in the notification settings
* Sort this query to the top
* Configure a saved query *All issues* (empty query)
* Select **Do not watch** in the notification settings
* Sort this query to the second place

### No notifications at all

* Configure a saved query *All issues* (empty query)
* Select **Do not watch** in the notification settings
* Sort this query to the top
