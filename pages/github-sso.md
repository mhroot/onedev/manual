### Usage Scenario

Login with GitHub account

### How to Set Up

1. Make sure your OneDev instance can be accessed publicly, and configure the public server url in _Administration / System Setting_

  ![Configure Server Url](../images/configure-server-url.png)
  
2. At GitHub side, register OneDev as a OAuth application under either a user account or an organization account

  ![Github Register Onedev1](../images/github/github-register-onedev1.png)
  
  ![Github Register Onedev2](../images/github/github-register-onedev2.png)
  
3. After registering, GitHub will display client id and secret for the application, which will be used later

  ![Github Register Onedev3](../images/github/github-register-onedev3.png)
  
4. At OneDev side, add a single sign on provider of type _OpenID (GitHub)_ in _Administration / Authentication Source / Single Sign On_ and fill in client id and secret shown above

  ![Add Github Sso](../images/github/add-github-sso.png)
  
5. Save the setting, click the definition to show details, and copy the callback URL

  ![Github Sso Details](../images/github/github-sso-details.png)

6. Now paste the copied callback URL into OneDev application registered at GitHub side, and save the setting

  ![Update Github Callback Url](../images/github/update-github-callback-url.png)
  
7. Logout OneDev and you will see a _Login with GitHub_ button at bottom of the login screen.  Any GitHub account with a **public email** will be able to login via this button now

  ![Github Login Button](../images/github/github-login-button.png)