1. [Diff Selection](diff-selection.md)
2. [Scripting](scripting.md)
3. [Job Variables](job-variables.md)
4. [Path Wildcard Match](path-wildcard.md)