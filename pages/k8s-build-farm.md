Meet Build Farm In Kubernetes Cluster
---

1. Deploy OneDev server into a k8s cluster following [this guide](deploy-into-k8s.md) (no need to set up ingress and letsencrypt for our demonstration purpose)

1. From OneDev projects page, add a project _my-app_

1. Run below command from your local machine to create a react application:

  ```
  npx create-react-app my-app
  ```
  
1. Change into directory _my-app_, and run below command to push code to OneDev:

  ```
  git remote add origin http://<onedev-external-ip>/my-app
  git push origin master:master
  ```
  When prompted, input administrator account specified above as git credential
  
1. Visit files page of project _my-app_ from OneDev, click link _add build spec_ to bring up the GUI to add build specification. For typical projects, OneDev suggests default job templates like below:

  ![Add Job Wizard](../images/add-job-wizard.png)
  
1. Just use the default template, and save the spec. Now you will see that a CI build is running as pods in your cluster. 

  ![After Add Ci Job](../images/after-add-ci-job.png)
    
1. Congrats! You've finished the tutorial. Continue to check [more tutorials](tutorials.md) if you are interested.